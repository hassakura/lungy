package com.example.hideki.tcc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class InfoActivity extends AppCompatActivity {

    private EditText etWeight;
    private EditText etHeight;
    private EditText etAge;
    private RadioGroup rgSex;
    private Button btDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        etWeight = findViewById(R.id.et_weight);
        etHeight = findViewById(R.id.et_height);
        etAge = findViewById(R.id.et_age);

        btDone = findViewById(R.id.btn_done);
        rgSex = findViewById(R.id.rg_sex);



        btDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etWeight.getText().toString().equals("") || etHeight.getText().toString().equals("")
                        || etAge.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Complete all fields!"
                            , Toast.LENGTH_LONG).show();
                }
                else {
                    SharedPreferences.Editor mEditor = sharedPref.edit();
                    mEditor.putString("weight_value", etWeight.getText().toString())
                            .apply();
                    mEditor.putString("height_value", etHeight.getText().toString())
                            .apply();
                    mEditor.putString("age_value", etAge.getText().toString()).apply();
                    if (rgSex.getCheckedRadioButtonId() == R.id.rb_male) sharedPref.edit().putString
                            ("sex_list", "1").apply();
                    else sharedPref.edit().putString("sex_list", "0").apply();
                    sharedPref.edit().putBoolean("firstrun", false).apply();
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                }

            }
        });

    }
}
