package com.example.hideki.tcc;

class PhysicalActivity {

    private static final double METhCyc = 6.8;
    private static final double METhWalk = 4.0;
    private static final double rrOfCycling = 0.87;
    private static final double rrOfWalk = 0.9;
    private static final double backgroundContrast = 11.25 / 7;
    private static final double powerLog = 0.5;
    private static final double backgroundApCycling = 2.0;
    private static final double backgroundApWalk = 1.1;
    private static final double rrForPM25 = 1.07;
    private static final double rrForPM10 = 1.045;
    private static final double ventRateSleep = 0.27;
    private static final double ventRateRest = 0.609;
    private static final double ventRateCycling = 2.55;
    private static final double ventRateWalk = 1.37;

    private static final double sleepTime = 8.0;
    private static final double restTime = 16.0;

    private float weight;
    private float height;
    private float age;
    private int sex;

    // PA = 0 WALK
    // PA = 1 CYC

    public int getPAType() {
        return PAType;
    }

    public void setPAType(int PAType) {
        this.PAType = PAType;
    }

    private int PAType;
    private double BreakevenPoint;
    private double pmConcentration;
    private double pm25Inhaled;
    private double pm10Inhaled;
    private int Pollutant;

    public double getAqiValue() {
        return aqiValue;
    }

    public void setAqiValue(double aqiValue) {
        this.aqiValue = aqiValue;
    }

    private double aqiValue;


    public double getPm25Inhaled() {
        return pm25Inhaled;
    }

    public void setPm25Inhaled(double polInhaled) {
        this.pm25Inhaled = polInhaled;
    }

    public double getPm10Inhaled() {
        return pm10Inhaled;
    }

    public void setPm10Inhaled(double polInhaled) {
        this.pm10Inhaled = polInhaled;
    }


    public PhysicalActivity(int PhysicalActivity, double aqiValue, int pol){
        this.aqiValue = aqiValue;
        this.pm25Inhaled = 0;
        this.pm10Inhaled = 0;
        this.Pollutant = pol;
        this.PAType = PhysicalActivity;
        if (Pollutant == 0) //pm25
            this.pmConcentration = aqi2micrograms(aqiValue);
        else
            this.pmConcentration = pm10Aqi2Micrograms(aqiValue);
        //this.BreakevenPoint = calcBreakevenPoint(pol);


    }

    public void setBreakevenPoint(){

        BreakevenPoint = calcBreakevenPoint(PAType, Pollutant);
    }

    // Em minutos
    public double getBreakevenPoint(){
        return BreakevenPoint;
    }

    public double getPmConcentration() {
        return pmConcentration;
    }

    public void setPmConcentration(double aqiValue) {
        this.pmConcentration = aqi2micrograms(aqiValue);
    }


    public double calcBreakevenPoint(int PAType, int Pollutant){
        double PATime = 0, pafRatio = 0, minute = 1.0 / 60;
        double pafOfPA = 0, pafOfPM = 0, METh;
        if (PAType == 0) METh = MetBalance(height, weight, age, sex, METhWalk);
        else METh = MetBalance(height, weight, age, sex, METhCyc);
        while (pafRatio < 1.00000 && PATime < 24){
            PATime  += minute;
            pafOfPA = calcPAF(PATime, METh, PAType);
            if (Pollutant == 0) pafOfPM = calcPAFpm25(PATime, PAType);
            else pafOfPM =calcPAFpm10(PATime, PAType);
            pafRatio = pafOfPA * pafOfPM;
        }

        return PATime * 60;
    }


    private double calcPAF (double timePA, double MetValue, int PAType){
        double METhPA, relativeRisk;
        if (PAType == 0){
            METhPA = timePA * MetValue;
            relativeRisk = Math.pow(Math.pow(rrOfWalk, 1.0 / Math.pow(backgroundContrast, powerLog)), Math.pow(METhPA, powerLog));
        }
        else{
            METhPA = timePA * MetValue;
            relativeRisk = Math.pow(Math.pow(rrOfCycling, 1.0 / Math.pow(backgroundContrast, powerLog)), Math.pow(METhPA, powerLog));
        }

        return (1 - (1 - relativeRisk));
    }


    private double calcPAFpm25 (double PATime, int PAType){
        double noTravelConcentration, pm25InhaledFunc, pm25Increase, relativeRisk;
        noTravelConcentration = pmConcentration * (ventRateSleep * sleepTime + ventRateRest * restTime);
        if (PAType == 0) {
            pm25InhaledFunc = pmConcentration * (ventRateSleep * sleepTime + ventRateRest * (restTime - PATime)
                    + backgroundApWalk * PATime * ventRateWalk) + pm25Inhaled;
        }
        else{
            pm25InhaledFunc = pmConcentration * (ventRateSleep * sleepTime + ventRateRest * (restTime - PATime)
                    + backgroundApCycling * PATime * ventRateCycling) + pm25Inhaled;
        }
        pm25Increase = pmConcentration * (pm25InhaledFunc / noTravelConcentration - 1);
        relativeRisk = Math.pow(Math.E, (Math.log(rrForPM25) * pm25Increase / 10));
        return (1 - (1 - relativeRisk));
    }


    private double calcPAFpm10 (double PATime, int PAType){
        double noTravelConcentration, pm10InhaledFunc, pm10Increase, relativeRisk;
        noTravelConcentration = pmConcentration * (ventRateSleep * sleepTime + ventRateRest * restTime);
        if (PAType == 0) {
            pm10InhaledFunc = pmConcentration * (ventRateSleep * sleepTime + ventRateRest * (restTime - PATime)
                    + backgroundApWalk * PATime * ventRateWalk) + pm10Inhaled;
        }
        else{
            pm10InhaledFunc = pmConcentration * (ventRateSleep * sleepTime + ventRateRest * (restTime - PATime)
                    + backgroundApCycling * PATime * ventRateCycling) + pm10Inhaled;
        }
        pm10Increase = pmConcentration * (pm10InhaledFunc / noTravelConcentration - 1);
        relativeRisk = Math.pow(Math.E, (Math.log(rrForPM10) * pm10Increase / 10));
        return (1 - (1 - relativeRisk));
    }


    @Override
    public String toString() {
        int hours, minutes;
        hours = (int) Math.ceil(BreakevenPoint) / 60;
        minutes = (int) Math.ceil(BreakevenPoint) % 60;
        return (Integer.toString(hours) + ":" + Integer.toString(minutes) + ":" + "00");
    }

    public String bepToString(double time){
        int hours, minutes;
        hours = (int) Math.ceil(time) / 60;
        minutes = (int) Math.ceil(time) % 60;
        return (Integer.toString(hours) + ":" + Integer.toString(minutes) + ":" + "00");
    }

    public double pollutionInhaled(double seconds, double pmConcentration){
        if (PAType == 1) return pmConcentration * backgroundApCycling * seconds / 3600 * ventRateCycling;
        else return pmConcentration * backgroundApWalk * seconds / 3600 * ventRateWalk;
    }

    public double aqi2micrograms(double aqiValue){
        if (aqiValue > 400) return (aqiValue - 401) * 149.9 / 99.0 + 350.5;
        else if (aqiValue > 300) return (aqiValue - 301) * 99.9 / 99.0 + 250.5;
        else if (aqiValue > 200) return (aqiValue - 201) * 99.9 / 99.0 + 150.5;
        else if (aqiValue > 150) return (aqiValue - 151) * 94.9 / 49 + 55.5;
        else if (aqiValue > 100) return (aqiValue - 101) * 19.9 / 49 + 35.5;
        else if (aqiValue > 50) return (aqiValue - 51) * 23.3 / 49 + 12.1 ;
        else return  (aqiValue) * 12 / 50;
    }

    public double pm10Aqi2Micrograms(double aqiValue){
        if (aqiValue > 400) return (aqiValue - 401) * 99.0 / 99.0 + 505;
        else if (aqiValue > 300) return (aqiValue - 301) * 79.0 / 99.0 + 425;
        else if (aqiValue > 200) return (aqiValue - 201) * 69.0 / 99.0 + 355;
        else if (aqiValue > 150) return (aqiValue - 151) * 99.0 / 49 + 255;
        else if (aqiValue > 100) return (aqiValue - 101) * 99.0 / 49 + 155;
        else if (aqiValue > 50) return (aqiValue - 51) * 99.0 / 49 + 55 ;
        else return  (aqiValue) * 54 / 50;
    }

    // Female = 0, Male = 1

    private Double MetBalance(float height, float weight, float age, int sex, double MetValue){
        double Hb;
        if (sex == 0) {
            Hb = 655.0955 + 1.8496 * height + 9.5634 * weight - 4.6756 * age;
            Hb = convertKcalday2Mlkgmin(Hb, weight);
            return MetValue * 3.5 / Hb;
        }
	    else {
            Hb = 66.4730 + 5.0033 * height + 13.7516 * weight - 6.7550 * age;
            Hb = convertKcalday2Mlkgmin(Hb, weight);
            return MetValue * 3.5 / Hb;
        }
    }

    private double convertKcalday2Mlkgmin(Double kcals, float weight){
        return (kcals * 1000) / (1440 * 5 * weight);
    }

    public void setPersonInfo(float weight, float height, float age, int sex){
        this.weight = weight;
        this.height = height;
        this.age = age;
        this.sex = sex;
    }
}