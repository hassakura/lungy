package com.example.hideki.tcc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

import java.util.Random;

public class StartActivity extends AppCompatActivity {

    private Handler h = new Handler();
    private Random r = new Random();
    private long rnd_delay = (long) ((3 + (2 * r.nextDouble()))* 1000);
    private Runnable runnable;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        if (!isTaskRoot()
                && getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
                && getIntent().getAction() != null
                && getIntent().getAction().equals(Intent.ACTION_MAIN)) {

            finish();
            return;
        }


    }

    @Override
    protected void onResume(){
        super.onResume();
        System.out.println(rnd_delay);

        h.postDelayed( runnable = new Runnable() {
            public void run() {
                h.postDelayed(runnable, rnd_delay);
                if (sharedPref.getBoolean("firstrun", true)){
                    startActivity(new Intent(getApplicationContext(), InfoActivity.class));
                    h.removeCallbacksAndMessages(null);
                    finish();
                }
                else{
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    h.removeCallbacksAndMessages(null);
                    finish();
                }

            }
        }, rnd_delay);


    }

}
