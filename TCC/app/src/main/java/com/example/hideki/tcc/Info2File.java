package com.example.hideki.tcc;


import java.io.Serializable;
import java.util.ArrayList;


public class Info2File implements Serializable{
    private ArrayList<Station> stations;

    public Info2File(){
        this.stations = new ArrayList<>();
    }

    public void addStation(Station s){
        stations.add(s);
    }

    public long infoLength(){
        return stations.size();
    }

    public ArrayList<Station> getStations() {
        return stations;
    }

    public void readStations(){
        for (Station i : stations){
            System.out.println(i.getData().getCity().getName() + " | " + i.getData().getAqi());
        }
    }
}
