package com.example.hideki.tcc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Station implements Serializable{

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    class Data implements Serializable{

        @SerializedName("aqi")
        @Expose
        private Integer aqi;
        @SerializedName("idx")
        @Expose
        private Integer idx;
        @SerializedName("attributions")
        @Expose
        private List<Attribution> attributions = null;
        @SerializedName("city")
        @Expose
        private City city;
        @SerializedName("dominentpol")
        @Expose
        private String dominentpol;
        @SerializedName("iaqi")
        @Expose
        private Iaqi iaqi;
        @SerializedName("time")
        @Expose
        private Time time;

        public Data(Integer aqi, Integer idx, List<Attribution> attributions, City city, String
                dominentpol, Iaqi iaqi, Time time) {
            super();
            this.aqi = aqi;
            this.idx = idx;
            this.attributions = attributions;
            this.city = city;
            this.dominentpol = dominentpol;
            this.iaqi = iaqi;
            this.time = time;
        }


        public Integer getAqi() {
            return aqi;
        }

        public void setAqi(Integer aqi) {
            this.aqi = aqi;
        }

        public Integer getIdx() {
            return idx;
        }

        public void setIdx(Integer idx) {
            this.idx = idx;
        }

        public List<Attribution> getAttributions() {
            return attributions;
        }

        public void setAttributions(List<Attribution> attributions) {
            this.attributions = attributions;
        }

        public City getCity() {
            return city;
        }

        public void setCity(City city) {
            this.city = city;
        }

        public String getDominentpol() {
            return dominentpol;
        }

        public void setDominentpol(String dominentpol) {
            this.dominentpol = dominentpol;
        }

        public Iaqi getIaqi() {
            return iaqi;
        }

        public void setIaqi(Iaqi iaqi) {
            this.iaqi = iaqi;
        }

        public Time getTime() {
            return time;
        }

        public void setTime(Time time) {
            this.time = time;
        }

        class Attribution implements Serializable{

            @SerializedName("url")
            @Expose
            private String url;
            @SerializedName("name")
            @Expose
            private String name;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

        }

        class City implements Serializable{

            @SerializedName("geo")
            @Expose
            private List<Double> geo = null;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("url")
            @Expose
            private String url;

            public List<Double> getGeo() {
                return geo;
            }

            public void setGeo(List<Double> geo) {
                this.geo = geo;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

        }


        class H implements Serializable{

            @SerializedName("v")
            @Expose
            private Double v;

            public Double getV() {
                return v;
            }

            public void setV(Double v) {
                this.v = v;
            }

        }

        class Iaqi implements Serializable{

            @SerializedName("h")
            @Expose
            private H h;
            @SerializedName("no2")
            @Expose
            private No2 no2;
            @SerializedName("o3")
            @Expose
            private O3 o3;
            @SerializedName("p")
            @Expose
            private P p;
            @SerializedName("pm25")
            @Expose
            private Pm25 pm25;
            @SerializedName("t")
            @Expose
            private T t;
            @SerializedName("pm10")
            @Expose
            private Pm10 pm10;
            @SerializedName("co")
            @Expose
            private Co co;
            @SerializedName("so2")
            @Expose
            private So2 so2;

            public H getH() {
                return h;
            }

            public void setH(H h) {
                this.h = h;
            }

            public So2 getSo2() {
                return so2;
            }

            public void setSo2(So2 so2) {
                this.so2 = so2;
            }

            public No2 getNo2() {
                return no2;
            }

            public void setNo2(No2 no2) {
                this.no2 = no2;
            }

            public O3 getO3() {
                return o3;
            }

            public void setO3(O3 o3) {
                this.o3 = o3;
            }

            public P getP() {
                return p;
            }

            public void setP(P p) {
                this.p = p;
            }

            public Pm25 getPm25() {
                return pm25;
            }

            public void setPm25(Pm25 pm25) {
                this.pm25 = pm25;
            }

            public Pm10 getPm10() {
                return pm10;
            }

            public void setPm10(Pm10 pm10) {
                this.pm10 = pm10;
            }

            public T getT() {
                return t;
            }

            public void setT(T t) {
                this.t = t;
            }

            public Co getCo() {
                return co;
            }

            public void setCo(Co co) {
                this.co = co;
            }

        }

        class So2 implements Serializable{

            @SerializedName("v")
            @Expose
            private Double v;

            public Double getV() {
                return v;
            }

            public void setV(Double v) {
                this.v = v;
            }

        }

        class No2 implements Serializable{

            @SerializedName("v")
            @Expose
            private Double v;

            public Double getV() {
                return v;
            }

            public void setV(Double v) {
                this.v = v;
            }

        }

        class Co implements Serializable{

            @SerializedName("v")
            @Expose
            private Double v;

            public Double getV() {
                return v;
            }

            public void setV(Double v) {
                this.v = v;
            }

        }

        class O3 implements Serializable{

            @SerializedName("v")
            @Expose
            private Double v;

            public Double getV() {
                return v;
            }

            public void setV(Double v) {
                this.v = v;
            }

        }

        class P implements Serializable{

            @SerializedName("v")
            @Expose
            private Double v;

            public Double getV() {
                return v;
            }

            public void setV(Double v) {
                this.v = v;
            }

        }

        class Pm25 implements Serializable{

            @SerializedName("v")
            @Expose
            private Double v;

            public Double getV() {
                return v;
            }

            public void setV(Double v) {
                this.v = v;
            }

        }

        class Pm10 implements Serializable{

            @SerializedName("v")
            @Expose
            private Double v;

            public Double getV() {
                return v;
            }

            public void setV(Double v) {
                this.v = v;
            }

        }

        class T implements Serializable{

            @SerializedName("v")
            @Expose
            private Double v;

            public Double getV() {
                return v;
            }

            public void setV(Double v) {
                this.v = v;
            }

        }

        class Time implements Serializable{

            @SerializedName("s")
            @Expose
            private String s;
            @SerializedName("tz")
            @Expose
            private String tz;
            @SerializedName("v")
            @Expose
            private Integer v;

            public String getS() {
                return s;
            }

            public void setS(String s) {
                this.s = s;
            }

            public String getTz() {
                return tz;
            }

            public void setTz(String tz) {
                this.tz = tz;
            }

            public Integer getV() {
                return v;
            }

            public void setV(Integer v) {
                this.v = v;
            }

        }
    }


}