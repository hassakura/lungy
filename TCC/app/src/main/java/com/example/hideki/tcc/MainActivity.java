package com.example.hideki.tcc;

import android.Manifest;

import android.animation.LayoutTransition;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Pair;
import android.view.MenuInflater;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class MainActivity extends AppCompatActivity {

    private static AppCompatActivity instance;
    //  Station
    private TextView tvStation;
    private ImageButton btnHeadline;

    //  AQI Bar
    private TextView tvAqiLevel, tvAqiStatus;
    private ImageView ivHelpAqi;
    private ProgressBar pbAqi;

    //  Information about Pollutant and Forecast

    //  Pollutant
    private TextView tvPM25;
    private TextView tvPM10;
    //  Forecast
    private TextView tvTemp;
    private TextView tvHum;
    private TextView tvPres;

    //  PAs before play pressed
    private ImageButton btnWalk;
    private ImageButton btnCyc;
    private TextView tvWalkBep;
    private TextView tvCycBep;
    private TextView tvWalkLimitText;
    private TextView tvCycLimitText;
    private ConstraintLayout cycLayout;
    private ConstraintLayout walkLayout;

    //  PAs after play
    private ImageButton btnWalkPlay;
    private ImageButton btnCycPlay;
    private TextView tvWalkBepPlay;
    private TextView tvCycBepPlay;
    private TextView tvCycLimitTextPlay;
    private TextView tvWalkLimitTextPlay;
    private ConstraintLayout cycPlayLayout;
    private ConstraintLayout walkPlayLayout;

    //  Chronometer
    private ProgressBar pbChronometer;
    private ConstraintLayout chronLayout;
    private TextView tvPctPb;
    private Chronometer mChronometer;

    //  Information about PA and pol inhalation

    private TextView tvDistance;
    private TextView tvPM25Inh;
    private TextView tvPM10Inh;

    // Buttons bottom
    private Button btnStart;
    private Button btnStop;
    private Button btnPause;
    private Button btnResume;

    //  Images Icons

    ImageView ivWarning;
    ImageView ivHelpPm10;
    ImageView  ivHelpPm25;
    ImageView  ivGoodMood;
    ImageView  ivModerateMood;
    ImageView  ivTempAlert;
    ImageView  ivTimeAlert;
    ImageView   ivPolAlert;


    private LocationManager locationManager;





    private LocationListener locationListener = null;



    private ConstraintLayout mainLayout;






    private PhysicalActivity PA;

    private OkHttpClient client;
    private MediaType JSON;

    private Gson gson;

    private double lastAqiValue = -1;
    private double totalDistance = 0;
    private double totalPm25Inh = 0;
    private double totalPm10Inh = 0;
    private int PAType = 1;
    private int PolType = -1;

    private Location l;

    private Station s = new Station();
    private Station lastKnownStation;

    private Handler h = new Handler();
    private int delay = 10*1000; // 10 seconds
    private Runnable runnable;

    private long timeStart = 0;
    private long timeStop = 0;

    private boolean networkEnabled;
    private boolean started = false;

    private SharedPreferences sharedPref;
    private Info2File historyStations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ((ViewGroup) findViewById(R.id.main_constraint)).getLayoutTransition()
                    .enableTransitionType(LayoutTransition.CHANGING);
        }

        instance = this;
        historyStations = new Info2File();

        initViews();

        initLocMan();

        initPAButtonsActions();

        initBtnActions();

        btnCyc.setColorFilter(getResources().getColor(R.color.white));
        tvCycLimitText.setTextColor(getResources().getColor(R.color.white));
        tvCycBep.setTextColor(getResources().getColor(R.color.white));
        /**/

        client = new OkHttpClient();
        JSON = MediaType.parse("application/json; charset=utf-8");
    }


    @Override
    protected void onResume(){
        super.onResume();
        h.postDelayed( runnable = new Runnable() {
            public void run() {
                reqPermission();
                h.postDelayed(runnable, delay);
                if (started) h.removeCallbacksAndMessages(null);
            }
        }, delay);


    }

    private void initBtnActions(){
        btnHeadline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });

        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeStop = SystemClock.elapsedRealtime() - mChronometer.getBase();
                stopGPS(locationListener);
                btnPause.setVisibility(View.GONE);
                btnResume.setVisibility(View.VISIBLE);
                started = false;
            }
        });

        btnResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChronometer.setBase(SystemClock.elapsedRealtime() - timeStop);
                timeStop = 0;
                initLocMan();
                mChronometer.start();
                btnResume.setVisibility(View.GONE);
                btnPause.setVisibility(View.VISIBLE);
                started = true;
            }
        });



        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (s.getStatus() != null){
                    totalDistance = 0.0;
                    totalPm10Inh = 0.0;
                    totalPm25Inh = 0.0;
                    timeStart = 0;
                    if (PA != null) {
                        PA.setPm10Inhaled(0.0);
                        PA.setPm25Inhaled(0.0);
                    }
                    initLocMan();
                    mChronometer.setBase(SystemClock.elapsedRealtime());
                    mChronometer.start();
                    pbChronometer.setProgress(pbChronometer.getMax());
                    started = true;
                    mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                        @Override
                        public void onChronometerTick(Chronometer chronometer) {
                            long secondsElapsed = (SystemClock.elapsedRealtime() - mChronometer.getBase()) / 1000;
                            if (secondsElapsed % 10 == 0) {
                                if (secondsElapsed % 30 == 0) {
                                    Map<String, String> LatLon;
                                    LatLon = getLocation();
                                    GetTask task = new GetTask();

                                    task.execute("https://api.waqi.info/feed/geo:" + LatLon.get("lat") + ";"
                                            + LatLon.get("lon") +
                                            "/?token=707e67b6f455876d837b757c8146a16f39dec375");


                                }
                                if (PA != null) setProgressBarProgress();
                            }
                            if (PA != null) {
                                if (PA.getBreakevenPoint() - secondsElapsed / 60 < 20)
                                    ivTimeAlert.setVisibility(View.VISIBLE);
                                else ivTimeAlert.setVisibility(View.GONE);
                            }
                        }
                    });
                    tvDistance.setText(R.string.icons_km_initial);
                    tvPM25Inh.setText(R.string.icons_migm3_pm25_initial);
                    tvPM10Inh.setText(R.string.icons_migm3_pm10_initial);
                    btnStart.setVisibility(View.GONE);
                    cycLayout.setVisibility(View.GONE);
                    walkLayout.setVisibility(View.GONE);
                    cycPlayLayout.setVisibility(View.VISIBLE);
                    walkPlayLayout.setVisibility(View.VISIBLE);
                    chronLayout.setVisibility(View.VISIBLE);
                    btnPause.setVisibility(View.VISIBLE);
                    btnStop.setVisibility(View.VISIBLE);
                }



            }
        });
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopGPS(locationListener);
                btnResume.setVisibility(View.GONE);
                btnPause.setVisibility(View.GONE);
                btnStop.setVisibility(View.GONE);
                btnStart.setVisibility(View.VISIBLE);
                updatePolInhaled();
                started = false;
                try {
                    writeToFileOutput(historyStations, "test_file");
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        });

        ivWarning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Warning!")
                        .setMessage(R.string.warning_help_text)
                        .setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        ivHelpAqi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Air Quality Index (AQI)")
                        .setMessage(R.string.aqi_help_info_text)
                        .setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        ivHelpPm10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("PM10 (coarse particles)")
                        .setMessage(R.string.pm10_help_info_text)
                        .setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        ivHelpPm25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("PM2.5 (fine particles)")
                        .setMessage(R.string.pm25_help_info_text)
                        .setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        ivModerateMood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Moderate")
                        .setMessage(R.string.moderate_help_text)
                        .setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        ivGoodMood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Good")
                        .setMessage(R.string.good_help_text)
                        .setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        ivPolAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("CAUTION!")
                        .setMessage(R.string.high_pollution_help_text)
                        .setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        ivTempAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("CAUTION!")
                        .setMessage(R.string.high_hum_help_text)
                        .setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        ivTimeAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("CAUTION!")
                        .setMessage(R.string.twenty_minutes_warning_text)
                        .setPositiveButton("Got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
    }

    private void initViews(){
        btnStart = findViewById(R.id.btn_start);
        btnStop = findViewById(R.id.btn_stop);

        btnCyc = findViewById(R.id.btn_cyc);
        btnWalk = findViewById(R.id.btn_walk);
        btnPause = findViewById(R.id.btn_pause);
        btnResume = findViewById(R.id.btn_resume);
        btnHeadline = findViewById(R.id.btn_headline);
        btnWalkPlay = findViewById(R.id.btn_walk_play);
        btnCycPlay = findViewById(R.id.btn_cyc_play);


        tvWalkLimitText = findViewById(R.id.tv_time_limit_walk);
        tvCycLimitText = findViewById(R.id.tv_time_limit_cyc);
        tvCycLimitTextPlay = findViewById(R.id.tv_time_limit_cyc_play);
        tvWalkLimitTextPlay = findViewById(R.id.tv_time_limit_walk_play);

        tvWalkBep = findViewById(R.id.tv_walk_bep);
        tvCycBep = findViewById(R.id.tv_cyc_bep);
        tvWalkBepPlay = findViewById(R.id.tv_walk_bep_play);
        tvCycBepPlay = findViewById(R.id.tv_cyc_bep_play);

        tvDistance = findViewById(R.id.tv_chron_km_val);
        tvPM25Inh = findViewById(R.id.tv_chron_pm25_val);
        tvPM10Inh = findViewById(R.id.tv_chron_pm10_val);

        tvStation = findViewById(R.id.tv_station);
        tvAqiLevel = findViewById(R.id.tv_aqi_value);
        tvAqiStatus = findViewById(R.id.tv_aqi_status);

        tvPM25 = findViewById(R.id.tv_pm25_val);
        tvPM10 = findViewById(R.id.tv_pm10_val);
        tvTemp = findViewById(R.id.tv_temp_val);
        tvHum = findViewById(R.id.tv_hum_val);
        tvPres = findViewById(R.id.tv_pres_val);

        ivWarning = findViewById(R.id.iv_warning);
        ivHelpAqi = findViewById(R.id.iv_help_aqi);
        ivHelpPm10 = findViewById(R.id.iv_help_pm10);
        ivHelpPm25 = findViewById(R.id.iv_help_pm25);
        ivGoodMood = findViewById(R.id.iv_good_mood);
        ivModerateMood = findViewById(R.id.iv_moderate_mood);
        ivTempAlert = findViewById(R.id.iv_temp_alert);
        ivTimeAlert = findViewById(R.id.iv_time_alert);
        ivPolAlert = findViewById(R.id.iv_pol_alert);


        mChronometer = findViewById(R.id.chron);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        pbAqi = findViewById(R.id.pb_aqi);
        tvPctPb = findViewById(R.id.tv_pct_pb);
        pbChronometer = findViewById(R.id.pb_chron);
        mainLayout = findViewById(R.id.main_constraint);

        cycLayout = findViewById(R.id.cl_btn_cyc);
        walkLayout = findViewById(R.id.cl_btn_walk);
        cycPlayLayout = findViewById(R.id.cl_btn_cyc_play);
        walkPlayLayout = findViewById(R.id.cl_btn_walk_play);
        chronLayout = findViewById(R.id.cl_chron);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.navigation, menu);
        return true;
    }

    private void setProgressBarProgress(){
        long Bep = (long) PA.getBreakevenPoint() * 60;
        long actualTimePassed = (SystemClock.elapsedRealtime() - mChronometer.getBase()) / 1000;
        int rating = (int) (10000 * actualTimePassed/ Bep);
        float pctRating = (float) actualTimePassed/ Bep;
        if (rating <= 10000){
            pbChronometer.setProgress(10000 - rating);
            mChronometer.setTextColor(Color.BLACK);
            System.out.println(String.format(Locale.ENGLISH,"%.1f", (pctRating * 100)));
            System.out.println("pct: " + pctRating);
            tvPctPb.setText(String.format(Locale.ENGLISH,"%.1f%%", (100 - (pctRating * 100))));
        }
        else{
            if (pbChronometer.getProgress() > 0) pbChronometer.setProgress(0);
            mChronometer.setTextColor(getResources().getColor(R.color.aqi150));
        }

    }

    private void changePAButtonsColor(int PaSelectec){
        switch (PaSelectec){ // Walk selected
            case 0:
                PAType = 0;
                btnCyc.setColorFilter(getResources().getColor(R.color.gray500));
                tvCycBep.setTextColor(getResources().getColor(R.color.gray500));
                tvCycLimitText.setTextColor(getResources().getColor(R.color.gray500));
                btnWalk.setColorFilter(getResources().getColor(R.color.white));
                tvWalkBep.setTextColor(getResources().getColor(R.color.white));
                tvWalkLimitText.setTextColor(getResources().getColor(R.color.white));
                btnCycPlay.setColorFilter(getResources().getColor(R.color.gray500));
                tvCycBepPlay.setTextColor(getResources().getColor(R.color.gray500));
                tvCycLimitTextPlay.setTextColor(getResources().getColor(R.color.gray500));
                btnWalkPlay.setColorFilter(getResources().getColor(R.color.white));
                tvWalkBepPlay.setTextColor(getResources().getColor(R.color.white));
                tvWalkLimitTextPlay.setTextColor(getResources().getColor(R.color.white));
                break;
            case 1: //Cycling
                PAType = 1;
                btnCyc.setColorFilter(getResources().getColor(R.color.white));
                tvCycBep.setTextColor(getResources().getColor(R.color.white));
                tvCycLimitText.setTextColor(getResources().getColor(R.color.white));
                btnWalk.setColorFilter(getResources().getColor(R.color.gray500));
                tvWalkBep.setTextColor(getResources().getColor(R.color.gray500));
                tvWalkLimitText.setTextColor(getResources().getColor(R.color.gray500));
                btnCycPlay.setColorFilter(getResources().getColor(R.color.white));
                tvCycBepPlay.setTextColor(getResources().getColor(R.color.white));
                tvCycLimitTextPlay.setTextColor(getResources().getColor(R.color.white));
                btnWalkPlay.setColorFilter(getResources().getColor(R.color.gray500));
                tvWalkBepPlay.setTextColor(getResources().getColor(R.color.gray500));
                tvWalkLimitTextPlay.setTextColor(getResources().getColor(R.color.gray500));
                break;
            default:
                System.out.println("PA nao encontrada");
        }

    }

    private void initPAButtonsActions(){
        btnWalk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePAButtonsColor(0);
                if (PA != null && started){
                    PA.setPAType(PAType);
                    setProgressBarProgress();
                }
            }
        });
        btnCyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePAButtonsColor(1);
                if (PA != null && started){
                    PA.setPAType(PAType);
                    setProgressBarProgress();
                }
            }
        });
        btnWalkPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePAButtonsColor(0);
                if (PA != null && started){
                    PA.setPAType(PAType);
                    setProgressBarProgress();
                }
            }
        });
        btnCycPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePAButtonsColor(1);
                if (PA != null && started){
                    PA.setPAType(PAType);
                    setProgressBarProgress();
                }
            }
        });
    }

    private void updateTextInfo(){
        if (s.getData().getIaqi().getPm25() != null) tvPM25.setText(String.format(Locale.ENGLISH, "%.2f", PA.aqi2micrograms(s.getData().getIaqi().getPm25().getV())));
        else tvPM25.setText("--");
        if (s.getData().getIaqi().getPm10() != null) tvPM10.setText(String.format(Locale.ENGLISH, "%.2f",PA.pm10Aqi2Micrograms(s.getData().getIaqi().getPm10().getV())));
        else tvPM10.setText("--");
        if (s.getData().getIaqi().getT() != null) tvTemp.setText(String.valueOf(s.getData().getIaqi().getT().getV().longValue()));
        else tvTemp.setText("--");
        if (s.getData().getIaqi().getH() != null) tvHum.setText(String.valueOf(s.getData().getIaqi().getH().getV().longValue()));
        else tvHum.setText("--");
        if (s.getData().getIaqi() != null) tvPres.setText(String.valueOf(s.getData().getIaqi().getP().getV().longValue()));
        else tvPres.setText("--");
    }

    private void reqPermission() {
        try {

            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else {
                Map<String, String> LatLon;
                LatLon = getLocation();

                if (LatLon != null) {
                    GetTask task = new GetTask();
                    task.execute("https://api.waqi.info/feed/geo:" + LatLon.get("lat") + ";"
                            + LatLon.get("lon") + "/?token=707e67b6f455876d837b757c8146a16f39dec375");

                } else
                    Toast.makeText(getApplicationContext(), getString(R.string.connection_failed), Toast.LENGTH_LONG).show(); //Colocar em string

            }
            ;
        }
        catch(SecurityException ex){
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void initLocMan(){
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        ArrayList<Pair<Location, Double>> la = new ArrayList<>();
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (ContextCompat.checkSelfPermission(MainActivity.super.getApplicationContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED && started){
                    totalDistance += l.distanceTo(location);
                    tvDistance.setText(getString(R.string.icons_km, totalDistance / 1000.0));

                    l = location;
                    if (PA != null) updatePolInhaled();
                }
            }

            @Override
            public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
            }

            @Override
            public void onProviderEnabled(String arg0) {
            }

            @Override
            public void onProviderDisabled(String arg0) {
            }
        };
        networkEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED){
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            l = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }

    }

    private Map<String, String> getLocation() {
        try{
            if (networkEnabled) {
                final Map<String, String> LatLon = new HashMap<>();
                Location l = locationManager.getLastKnownLocation(LocationManager
                        .GPS_PROVIDER);
                LatLon.put("lat", String.valueOf(l.getLatitude()));
                LatLon.put("lon", String.valueOf(l.getLongitude()));

                return LatLon;
            }
            else return null;
        }catch(SecurityException ex){
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
            return null;
        }
    }

    private void updatePolInhaled(){
        double timePassed = ((SystemClock.elapsedRealtime() - mChronometer.getBase()) -
                timeStart) / 1000.0;
        timeStart = (SystemClock.elapsedRealtime() - mChronometer.getBase());
        System.out.println("TP: " + timePassed);
        if (PolType == 0) {
            PA.setPm25Inhaled(PA.pollutionInhaled(timePassed, PA.getPmConcentration()) + totalPm25Inh);
            totalPm25Inh = PA.getPm25Inhaled();
            tvPM25Inh.setText(getString(R.string.icons_migm3_pm25, totalPm25Inh));
        }
        else if (PolType == 1){
            PA.setPm10Inhaled(PA.pollutionInhaled(timePassed, PA.getPmConcentration()) + totalPm10Inh);
            totalPm10Inh = PA.getPm10Inhaled();
            tvPM10Inh.setText(getString(R.string.icons_migm3_pm10, totalPm10Inh));
        }
        else ;
    }


    private void setAqiColor(double aqiValue){
        pbAqi.setProgress((int) ((aqiValue / 300) * 7500));
        if (aqiValue > 300){
            tvAqiStatus.setBackgroundColor(getResources().getColor(R.color.aqi300));
            tvAqiStatus.setText("Hazardous");
            pbAqi.getProgressDrawable().setColorFilter(
                    getResources().getColor(R.color.aqi300), android.graphics.PorterDuff.Mode.SRC_IN);
            ivGoodMood.setVisibility(View.GONE);
            ivModerateMood.setVisibility(View.GONE);
            ivWarning.getDrawable().setTint(getResources().getColor(R.color.redA700));
            ivWarning.setVisibility(View.VISIBLE);
            ivPolAlert.setVisibility(View.VISIBLE);
        }
        else if (aqiValue > 200){
            tvAqiStatus.setText("Very Unhealthy");
            tvAqiStatus.setBackgroundColor(getResources().getColor(R.color.aqi200));
            pbAqi.getProgressDrawable().setColorFilter(
                    getResources().getColor(R.color.aqi200), android.graphics.PorterDuff.Mode.SRC_IN);
            ivGoodMood.setVisibility(View.GONE);
            ivModerateMood.setVisibility(View.GONE);
            ivWarning.getDrawable().setTint(getResources().getColor(R.color.redA700));
            ivWarning.setVisibility(View.VISIBLE);
            ivPolAlert.setVisibility(View.VISIBLE);
        }
        else if (aqiValue > 150){
            tvAqiStatus.setText("Unhealthy");
            tvAqiStatus.setBackgroundColor(getResources().getColor(R.color.aqi150));
            pbAqi.getProgressDrawable().setColorFilter(
                    getResources().getColor(R.color.aqi150), android.graphics.PorterDuff.Mode.SRC_IN);
            ivGoodMood.setVisibility(View.GONE);
            ivModerateMood.setVisibility(View.GONE);
            ivWarning.getDrawable().setTint(getResources().getColor(R.color.redA700));
            ivWarning.setVisibility(View.VISIBLE);
            ivPolAlert.setVisibility(View.VISIBLE);
        }
        else if (aqiValue > 100){
            tvAqiStatus.setText("Unhealthy for Sensitive Groups");
            tvAqiStatus.setBackgroundColor(getResources().getColor(R.color.aqi100));
            pbAqi.getProgressDrawable().setColorFilter(
                    getResources().getColor(R.color.aqi100), android.graphics.PorterDuff.Mode.SRC_IN);
            ivGoodMood.setVisibility(View.GONE);
            ivModerateMood.setVisibility(View.GONE);
            ivWarning.getDrawable().setTint(getResources().getColor(R.color.aqi50));
            ivWarning.setVisibility(View.VISIBLE);
            ivPolAlert.setVisibility(View.VISIBLE);
        }
        else if (aqiValue > 50){
            tvAqiStatus.setText("Moderate");
            tvAqiStatus.setBackgroundColor(getResources().getColor(R.color.aqi50));
            pbAqi.getProgressDrawable().setColorFilter(
                    getResources().getColor(R.color.aqi50), android.graphics.PorterDuff.Mode
                    .SRC_IN);
            ivWarning.setVisibility(View.GONE);
            ivModerateMood.setVisibility(View.VISIBLE);
            ivPolAlert.setVisibility(View.GONE);
        }
        else {
            tvAqiStatus.setText("Good");
            tvAqiStatus.setBackgroundColor(getResources().getColor(R.color.aqi0));
            pbAqi.getProgressDrawable().setColorFilter(
                    getResources().getColor(R.color.aqi0), android.graphics.PorterDuff.Mode.SRC_IN);
            ivWarning.setVisibility(View.GONE);
            ivGoodMood.setVisibility(View.VISIBLE);
            ivPolAlert.setVisibility(View.GONE);
        }
    }

    private void stopGPS(LocationListener listener){
        locationManager.removeUpdates(listener);
        mChronometer.stop();
    }

    private void tvStationFormat(String station){
        StringBuilder sb = new StringBuilder();
        String[] splittedString = station.split(",");
        String topString = "";
        for (int i = 0; i < splittedString.length - 2; i++)
            sb.append(splittedString[i]);
        topString = sb.toString();
        Spannable t1 = new SpannableString(topString);
        t1.setSpan(new ForegroundColorSpan(Color.BLACK), 0, t1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        Spannable t2 = new SpannableString(splittedString[splittedString.length - 2] +
                "," + splittedString[splittedString.length - 1]);
        t2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.gray300)), 0, t2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        t2.setSpan(new AbsoluteSizeSpan(12, true), 0, t2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvStation.setText(t1);
        tvStation.append("\n");
        tvStation.append(t2);
    }

    class GetTask extends AsyncTask<String, Void, String> {
        private Exception exception;

        protected String doInBackground(String... urls) {
            try {
                String getResponse = get(urls[0]);
                return getResponse;
            } catch (Exception e) {
                this.exception = e;
                return null;
            }
        }

        protected void onPostExecute(String getResponse) {
            try {
                gson = new Gson();
                try{
                    s = gson.fromJson(getResponse, Station.class);
                } catch (JsonParseException e){
                    System.out.println(e);
                }
                if (s.getStatus().equals("ok")) {
                    System.out.println(s.getData().getAqi());
                    System.out.println(s.getData().getIaqi().getPm25().getV());
                    historyStations.addStation(s);
                    lastKnownStation = s;
                    tvStationFormat(s.getData().getCity().getName());
                    if (lastAqiValue != -1 && lastAqiValue == s.getData().getAqi());
                    else{
                        if (s.getData().getIaqi().getPm25() != null){
                            PolType = 0;
                            tvAqiLevel.setText(String.format(Locale.ENGLISH, "%s", String.valueOf(s.getData().getIaqi().getPm25().getV().longValue())));
                            setAqiColor(s.getData().getIaqi().getPm25().getV().longValue());

                            PA = new PhysicalActivity(PAType, s.getData().getIaqi().getPm25().getV(), 0);
                            PA.setPersonInfo(Float.parseFloat(sharedPref.getString
                                            ("weight_value", "70")),
                                    Float.parseFloat(sharedPref.getString
                                            ("height_value",
                                                    "170")), Float.parseFloat(sharedPref.getString
                                            ("age_value", "30")),
                                    Integer.parseInt(sharedPref.getString
                                            ("sex_list", "0")));
                            PA.setBreakevenPoint();
                            PA.setPm25Inhaled(totalPm25Inh);
                            lastAqiValue = PA.getAqiValue();
                            updateBepText();
                        }
                        else if (s.getData().getIaqi().getPm10() != null){
                            PolType = 1;
                            tvAqiLevel.setText(String.format(Locale.ENGLISH, "%s", String.valueOf(s.getData().getIaqi().getPm10().getV().longValue())));
                            setAqiColor(s.getData().getIaqi().getPm10().getV().longValue());
                            PA = new PhysicalActivity(PAType, s.getData().getIaqi().getPm10().getV(), 1);
                            PA.setPersonInfo(Float.parseFloat(sharedPref.getString
                                            ("weight_value", "70")),
                                    Float.parseFloat(sharedPref.getString
                                            ("height_value",
                                            "170")), Float.parseFloat(sharedPref.getString
                                                    ("age_value", "30")),
                                    Integer.parseInt(sharedPref.getString
                                            ("sex_list", "0")));
                            PA.setBreakevenPoint();
                            PA.setPm10Inhaled(totalPm10Inh);
                            lastAqiValue = PA.getAqiValue();
                            updateBepText();

                        }
                        else ;
                        updateTextInfo();
                        if (started){
                            if (s.getData().getIaqi().getT().getV() > 28.0 && s.getData().getIaqi()
                                    .getH().getV() > 80)
                                ivTempAlert.setVisibility(View.VISIBLE);
                            else ivTempAlert.setVisibility(View.GONE);
                        }

                    }



                } else System.out.println(" Status: " + s.getStatus());
            } catch (Exception e){
                System.out.println(e);
            }
        }

        String get(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response = client.newCall(request).execute();
            return response.body().string();
        }
    }

    private void updateBepText(){
        double bepCyc = PA.calcBreakevenPoint(1, PolType);
        double bepWalk = PA.calcBreakevenPoint(0, PolType);
        if (bepCyc > (24 * 60)){
            tvCycBep.setText("Your Body");
            tvCycBepPlay.setText("Your Body");
        }
        else{
            tvCycBep.setText(PA.bepToString(bepCyc));
            tvCycBepPlay.setText(PA.bepToString(bepCyc));
        }
        if (bepWalk > (24 * 60)){
            tvWalkBep.setText("Your Body");
            tvWalkBepPlay.setText("Your Body");
        }
        else {
            tvWalkBep.setText(PA.bepToString(bepWalk));
            tvWalkBepPlay.setText(PA.bepToString(bepWalk));
        }
    }

    public native String stringFromJNI();

    private void writeToFileOutput(Info2File info, String Filename) throws IOException,
            ClassNotFoundException {

        File file = getFileStreamPath(Filename);

        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(info);
        oos.close();
        fos.close();

//Para Ler
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Info2File retorno = (Info2File) ois.readObject();
        retorno.readStations();
        fis.close();
        ois.close();

        /*  Para colocar as coisas, provavelmente terei que criar um Objeto Collection, e colocar
            nele uma classe serializable, pra gravar no arquivo.
         */
    }

    static {
        System.loadLibrary("native-lib");
    }

    public static Context getContext(){
        return instance.getApplicationContext();
    }


}
