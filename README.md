# Lungy

## Resumo

Muitos recursos tecnológicos são inventados toda hora, e pode-se usá-los para melhorar a vida de diversas pessoas. Nem todos os habitantes de centros urbanos têm consciência da qualidade do ar que respiram ou o mal que isso faz. Assim, foi desenvolvido um aplicativo Android chamado __Lungy__, que deve informar praticantes de atividades físicas, principalmente aqueles que moram em cidades poluídas, sobre a poluição local e o mal que poderá ocorrer ao se exercitar nesse tipo de ambiente. Com dados coletados por companhias especializadas no monitoramento da qualidade do ar, o usuário dispõe de medidas da concentração de material particulado e gases poluentes presentes no ar, condições climáticas e uma sugestão de quanto tempo é recomendado se exercitar no local, dependendo do tipo da atividade que está sendo praticada. Futuramente, o aplicativo poderá se integrar com outros dispositivos como Smartwatches e cintas cardíacas, para dar resultados ainda mais precisos. Com o avanço do conceito de Cidades Inteligentes, será mais frequente haver iniciativas desse tipo, tentando integrar cada vez mais as cidades e os cidadãos.

